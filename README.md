Prérequis : 

  - Robot Framework
  - TFParamService
  - (Optionnel même si préférable) Squash TM

Ce projet contient des fichiers robot séparés par licence pour tester la transmission des paramètres.

Les tests à exécuter se trouvent dans robot_default_params.robot et robot_params_special_characters.robot.

Les tests vérifient les valeurs des CUFs ou des DSs.

Pour qu'un test soit en succès, il faut insérer dans Squash TM les informations suivantes :

  - CPG_CUF_campaign : cpg[0] ^ %VAR% #<9:/&é"(-è_çà)=he | \pe(pe); $var

  - TS_CUF_testsuite : ts[0] ^ %VAR% #<9:/&é"(-è_çà)=he | \pe(pe); $var

  - TC_CUF_testcase : tc[0] ^ %VAR% #<9:/&é"(-è_çà)=he | \pe(pe); $var

  - IT_CUF_iteration : it[0] ^ %VAR% #<9:/&é"(-è_çà)=he | \pe(pe); $var

  - DSNAME : dsn[0] ^ %VAR% #<9:/&é"(-è_çà)=he | \pe(pe); $var

  - DS_FIRST_PARAM : ds[0] ^ %VAR% #<9:/&é"(-è_çà)=he | \pe(pe); $var


Le fichier robot_default_params.robot contient un test avec des valeurs par défaut.

Le fichier robot_params_special_characters.robot contient des tests des CUFs avec des valeurs contenant des caractères spéciaux pour les licences Community et Ultimate.

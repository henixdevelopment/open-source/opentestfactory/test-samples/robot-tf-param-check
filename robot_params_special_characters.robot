*** Settings ***
Documentation       Testing special characters for TFParamService in a Community or Ultimate environment

Library             squash_tf.TFParamService
Resource            keyword_definition.resource


*** Test Cases ***
Community Param Test
    Check Special Character Params Community

Ultimate Param Test
    Check Special Character Params Community
    Check Special Character Params Ultimate

*** Keywords ***

Check Special Character Params Community
    Parameter Value Should Be As Specified    TC_CUF_rf_testcase          tc[0] ^ %VAR% #<9:/&é"(-è_çà)=he | \\pe(pe); $var
    Parameter Value Should Be As Specified    DSNAME                      dsn[0] ^ %VAR% #<9:/&é"(-è_çà)=he | \\pe(pe); $var
    Parameter Value Should Be As Specified    DS_FIRST_PARAM              ds[0] ^ %VAR% #<9:/&é"(-è_çà)=he | \\pe(pe); $var

Check Special Character Params Ultimate
    Parameter Value Should Be As Specified    CPG_CUF_rf_campaign         cpg[0] ^ %VAR% #<9:/&é"(-è_çà)=he | \\pe(pe); $var
    Parameter Value Should Be As Specified    TS_CUF_rf_testsuite         ts[0] ^ %VAR% #<9:/&é"(-è_çà)=he | \\pe(pe); $var
    Parameter Value Should Be As Specified    IT_CUF_rf_iteration         it[0] ^ %VAR% #<9:/&é"(-è_çà)=he | \\pe(pe); $var


